import Vue from 'vue'
import Vuex from 'vuex'
import data from '@/locales/en.json';
import dataFr from '@/locales/fr.json';
// import VuexPersistence from 'vuex-persist'
// import createPersistedState from "vuex-persistedstate";

import { SCORM } from "pipwerks-scorm-api-wrapper";

Vue.use(Vuex)

// https://www.npmjs.com/package/vuex-persistedstate
// const vuexLocal = new VuexPersistence({
//     key: 'LDTstorage',
//     storage: window.localStorage
//   })

// https://flaviocopes.com/vuex-persist-localstorage/
// https://www.npmjs.com/package/vuex-persist
// const vuexLocal = new VuexPersistence({
//     key: 'LDTdata',
//     storage: session.localStorage
//   })

export const store = new Vuex.Store({
    // plugins: [vuexLocal.plugin],
    // plugins: [vuexPersist.plugin],
    // plugins: [createPersistedState({
    //     key: 'LDTdata',
    //     storage: window.sessionStorage
    // })],
    // plugins: [createPersistedState({
    //     key: 'LDTdata',
    // })],
    state: {
        data: data,
        dataFr: dataFr,
        cartItemCount: 0,
        cartItems: [],
        cartArchive: [],
        lmsAPI: {
            locale: "en",
            version: "1.2",
            active: false,
            suspendData: null
        }
    },
    // update the state
    mutations: {
        addToCart(state, payload) {
            debugger; // eslint-disable-line no-debugger
            let item = payload;
            item = { ...item, quantity: 1 }
            if(state.cartItems.length > 0) {
                let bool = state.cartItems.some(i => i.id === item.id)
                if (bool) {
                    let itemIndex = state.cartItems.findIndex(el => el.id === item.id)
                    // state.cartItems[itemIndex]["quantity"] += 1; add as many of each item as desired 
                    state.cartItems[itemIndex]["quantity"] = 1; //add only 1
                } else {
                    state.cartItems.push(item)
                }
             } else {
                state.cartItems.push(item)
            }
            state.cartItemCount++
        },
        addToArchive(state, payload) {
            debugger; // eslint-disable-line no-debugger
            let item = payload;
            item = { ...item, quantity: 1 }
            if(state.cartArchive.length > 0) {
                let bool = state.cartArchive.some(i => i.id === item.id)
                if (bool) {
                    let itemIndex = state.cartArchive.findIndex(el => el.id === item.id)
                    state.cartArchive[itemIndex]["quantity"] += 1; //this can't be =1 or only allows one item at a time in the archive array
                } 
                else {
                    state.cartArchive.push(item)
                }
             } else {
                state.cartArchive.push(item)
            }
        },
        removeItem(state, payload) {
            if(state.cartItems.length > 0) {
                let bool = state.cartItems.some(i => i.id === payload.id)

                if (bool) {
                    let index = state.cartItems.findIndex(el => el.id === payload.id)
                    if (state.cartItems[index]["quantity"] !== 0) {
                    state.cartItems[index]["quantity"] -= 1
                    state.cartItemCount--
                }
                if (state.cartItems[index]["quantity"] === 0) {
                    state.cartItems.splice(index, 1)
                }
            }
            }
        },
        
    },
    actions: {
        addToCart: (context, payload) => {
            context.commit("addToCart", payload);
        },
        addToArchive: (context, payload) => {
            context.commit("addToArchive", payload)
        },
        removeItem: (context, payload) => {
            context.commit("removeItem", payload)
        },
        
        com_init({ dispatch, state }) {
            SCORM.version = state.lmsAPI.version;

            // First LMS Call
            console.log("COM", "Initializing course. LMS API");
            var callSucceeded = false;
            if (state.lmsAPI.active) {
                callSucceeded = true;
            } else {
                callSucceeded = SCORM.init();
            }

            if (callSucceeded) {
                state.lmsAPI.active = true;

                switch (state.lmsAPI.version) {
                    case "1.2":
                        dispatch("com_set", { key: "cmi.exit", value: "suspend" });
                        break;
                    case "2004":
                        dispatch("com_set", { key: "cmi.exit", value: "suspend" });
                        // dispatch("com_return_suspendData", getters.com_get("cmi.suspend_data"));
                        break;
                }

                // SCORM.save();
            } else {
                console.log("COM", "API Error: API not found.");
            }
        },
        com_set({ state }, payload) {
            var key = payload.key;
            var value = payload.value;

            console.log("COM:", "Sending: '" + value + "'");

            var callSucceeded;
            if (state.lmsAPI.active) {
                callSucceeded = SCORM.set(key, value);    
            }
            else {
                callSucceeded = false;
            }
            if (!callSucceeded) {
                console.log("COM:", "Call failed? " + callSucceeded);
            }

            SCORM.save();
        },
        com_exit() {
            console.log("COM", "Terminating connection.");
            var callSucceeded = SCORM.quit();
            console.log("COM", "Call succeeded? " + callSucceeded);
            // if (callSucceeded) {
            //     setTimeout( () => {
            //         top.window.close();
            //     }, 3000);
            // }
        },
        com_setCompletion({ dispatch, state }, value) {
            switch (state.lmsAPI.version) {
                case "1.2":
                    dispatch("com_set", {
                        key: "cmi.core.lesson_status",
                        value: value,
                    });
                    break;
                case "2004":
                    dispatch("com_set", {
                        key: "cmi.completion_status",
                        value: value,
                    });
                    break;
            }
        },
        com_setSuccessStatus({ dispatch, state }, payload) {
            var value = payload.value;
            var set12Status = payload.set12Status;

            switch (state.lmsAPI.version) {
                case "1.2":
                    if (set12Status !== undefined && set12Status === true) {
                        dispatch("com_set", {
                            key: "cmi.core.lesson_status",
                            value: value,
                        });
                    }
                    break;
                case "2004":
                    dispatch("com_set", {
                        key: "cmi.success_status",
                        value: value,
                    });
                    break;
            }
        },
        com_update_suspendData({ dispatch, state }) {
            debugger; // eslint-disable-line no-debugger

            let arrayCartItems = [];
            let arrayCartArchive = [];

            // loop over cartItems
            state.cartItems.forEach(element => {
                var newElement = {
                    id: element.id
                }
                arrayCartItems.push(newElement);
            })

            // loop over cartArchive
            state.cartArchive.forEach(element => {
                var newElement = {
                    id: element.id
                }
                arrayCartArchive.push(newElement);
            })

            var _suspendData = {
                arrayCartItems: arrayCartItems,
                arrayCartArchive: arrayCartArchive
            }

            let stringVAR = JSON.stringify(_suspendData)
            dispatch("com_set", {
                key: "cmi.suspend_data",
                value: stringVAR
            });
        },
        com_return_suspendData({ state }, suspendData) {
            debugger; // eslint-disable-line no-debugger
            console.log("return_suspendData", suspendData);

            if ( suspendData && suspendData.length > 1 && suspendData != null && suspendData != "") {
                try {
                    var _suspendData = JSON.parse(suspendData);

                    console.log('state.lmsAPI.locale: ', state.lmsAPI.locale)
                    
                    var arrayAllCategories = [];
                    if (state.lmsAPI.locale == "fr") {
                        arrayAllCategories = state.dataFr.allCategories
                    }
                    else {
                        arrayAllCategories = state.data.allCategories
                    }
                    console.log('arrayAllCategories: ', arrayAllCategories)

                    // cartItems
                    if (typeof _suspendData.arrayCartItems !== "undefined") {
                        state.cartItems = [];
                        _suspendData.arrayCartItems.forEach(elementA => {
                            arrayAllCategories.forEach(elementB => {
                                if (elementA.id == elementB.id) {
                                    elementB.quantity = 1;
                                    state.cartItems.push(elementB);
                                }
                            });
                        });
                    }
                    console.log('state.cartItems: ', state.cartItems)

                    // cartArchive
                    if (typeof _suspendData.arrayCartArchive !== "undefined") {
                        state.cartArchive = [];
                        _suspendData.arrayCartArchive.forEach(elementA => {
                            arrayAllCategories.forEach(elementB => {
                                if (elementA.id == elementB.id) {
                                    state.cartArchive.push(elementB);
                                }
                            });
                        });
                    }
                    console.log('state.cartArchive: ', state.cartArchive)

                } catch (error) {
                    console.log("Found cmi.suspendData, but encountered error parsing JSON, will reset.");
                    console.warn(error);
                }
            }
        },
        com_setLanguage({ dispatch, state }, lang) {
            switch (state.lmsAPI.version) {
                case "1.2":
                    dispatch("com_set", {
                        key: "cmi.student_preference.language",
                        value: lang,
                    });
                    break;
                case "2004":
                    dispatch("com_set", {
                        key: "cmi.learner_preference.language",
                        value: lang,
                    });
                    break;
            }
        },
        com_setLocation({ dispatch, state }) {
            switch (state.lmsAPI.version) {
                case "1.2":
                    dispatch("com_set", {
                        key: "cmi.core.lesson_location",
                        value: "Home",
                    });
                    break;
                case "2004":
                    dispatch("com_set", {
                        key: "cmi.location",
                        value: "Home",
                    });
                    break;
            }
        },
    },
    getters: {
        com_get: (state) => (payload) => {
            if (state.lmsAPI.active) {
                console.log("COM", "Getting: '" + payload + "'");
                var returnValue = SCORM.get(payload);
                return returnValue;
            }
        },
        com_getLanguage(state, getters) {
            switch (state.lmsAPI.version) {
                case "1.2":
                    return getters.com_get("cmi.student_preference.language");
                case "2004":
                    return getters.com_get("cmi.learner_preference.language");
            }
        },
    }
})

export default store