# LCL Leadership Development Tool
---
## Intro
---
A custom application developed for LCL for emerging leaders. Designed based on an ecomm/shopping cart functionality, the app funnels users from a landing page, to a products library, then a final 'cart' displaying all chosen products with final download/checkout.  There is also LMS functionality archiving previous downloaded items for future reference.

## Details
---
The project uses vue, vuex, vue-router, SASS, vue-i18n for french translation, and is tailored for use on an LMS (`.xml` files are contained in `xml_files`, and router is configured in `router/index.js`). Animation was built custom using Intersection Observer and animate.css.

This application is not supported in IE.  An alert has been added to redirect users to a modern browser should they be trying to access it through Internet Explorer.

## LCL User Directions
---
This application has been built for relative ease-of-use and updating. The central area for adding content is the **Get Started** page, where Leadership capabilities can be added indefinitely (text, images, learning maps). The UI will automatically update with new additions.

---
## Project setup and updating

Go through the following steps to set up the development environment, install required dependencies, and make updates or additions to the content.

---

### Install node.js  

It is recommended to use the LTS version of Node which you can find on the node website: https://nodejs.org/en/.

Find notes about LTS versions and use in production [here](https://tamalweb.com/which-nodejs-version).


### Install project dependencies
```
cd into your project folder
run: $ npm install
```

### Spin up develpment server
```
$ npm run serve
```

### Build for production
```
$ npm run build
```

---

### **To add content to the project:**

-    open up the `src` folder
-    navigate to the `locales` folder (where all projecet text content lives)
-    select `en.json` (or `fr.json` if you are adding content in french first)
-    navigate down the page to the `allCategories` array. All the individual leadership capabilities and their related content are located here, numbered sequentially.
-    To add a new capability, go to the end of the array, copy an paste the last/previous object, then update all the fields as desired i.e.:

```JSON
    {
      "id": "13",
      "name": "New name goes here",
      "category":"New category goes here",
      "color": "New color goes here: bg-yellow = yellow; bg-green = green; bg-danger = red; bg-royal = royal blue",
      "img":"img/get_started/change.jpg",
      "imgAlt": "any related alt text goes here",
      "description":"New description goes here etc etc",
      "moreDetail": [
        "Seeing the necessity or opportunity created by change",  
        "Embracing discomfort and pursuing change with commitment",
        "Practicing resilience in the face of change and setbacks",
        "Proactively seeking out new challenges and opportunities to acquire new knowledge, perspectives and skills",
        "Leading others through organizational and cultural changes",
        "Removing barriers, managing resistance to change in others and demonstrating the benefits of change"
      ],
      "selectIf":[
        "You want to embrace and adapt to change",
        "You’d like to respond better to uncertainty, challenges and setbacks",
        "You need to continually and rapidly learn, unlearn, and relearn",
        "You need to lead organizational or cultural change",
        "You need to enable others to overcome resistance to change"
      ],
      "download": "./learning_maps/EN/LDT_Change_Agility.pdf"
    }
```

-    Images must be added manually to the `public/img` folder, and for new capabilities, they can be added to the `/get_started` sub folder. 
-    Image file paths use the `img/get_started/change.jpg` naming convention; copy and paste the new file name into the `"img"` key inside the JSON object
-    Learning maps must be made separately, then uploaded into the `public/learning_maps` folder.
-    Learning maps are separated into language folders, and use the `learning_maps/EN/LDT_Change_Agility.pdf` naming convention which is recommended to be kept for ease.

***Translations must be done separately:** Any new content requires translation, and can be pasted into the `fr.json` file. New languages can be added by creating a new `.json` file with the translated content, and named using a two-letter abbreviation for the language name i.e. Japanese = `ja.json`.

### **To rebuild and deploy the application with new content**

1.    Save all changes
2.    Run `npm run build` in the terminal
3.    This will create a `dist` folder in the root of the project
4.    Navigate to the `/dist` folder and zip it for upload to the LMS (this step may vary, and relate to the type of machine you are using) `zip -r LCL_LEADERSHIP_DEVELOPMENT_TOOL.zip ./ -x "*.git*" -x "*.DS_Store" -x "*.vscode*" -x "*.zip*" -x "*.xml_*" -x "*_xml_*" -x "__MACOSX*"`
5.    This will create a zip file inside the `dist` folder that will be ready for direct upload to SABA/SCORM.

---

#### Lints and fixes files
```
npm run lint
```